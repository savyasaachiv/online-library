# online-library

A backend API endpoint which uses flask framwwork to handle requests.

# Installation
Clone the url and run the server with flask or any wsgi application

# Example requests
Requests can be made to the hosted platform as shown below

validate and get ssid<br>
`curl -X POST "http://savyasaachi.pythonanywhere.com/auth" -d '{"user":"admin","password":"admin"}'`

logout session<br>
`curl -X POST "http://savyasaachi.pythonanywhere.com/logout" -d '{"SSID":"ssid-from-auth"}'`

get books<br>
`curl "http://savyasaachi.pythonanywhere.com/books?count=1"`

get books pdf by id<br>
`curl -X POST "http://savyasaachi.pythonanywhere.com/book_pdf" -d '{"SSID":"ssid-from-auth","Bid":1}'`

add books<br>
`curl -X PUT "http://savyasaachi.pythonanywhere.com/add" -d '{"SSID":"ssid-from-auth","URL":"http://url-of-book.com","name":"book name"}'`

remove books<br>
`curl -X DELETE "http://savyasaachi.pythonanywhere.com/remove' -d '{"SSID":"ssid-from-auth","Bid":number}'`
