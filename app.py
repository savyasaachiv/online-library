from flask import Flask, request, jsonify
import json
from mods import *


app=Flask(__name__)


@app.route('/')
def initFunc():
    routes={
            "routes":[
                "/auth",
                "/logout",
                "/books",
                "/book_pdf", #get pdf of book by its id
                "/add",
                "/remove",
                ]
            }

    return jsonify(routes)


@app.route('/auth', methods= ['POST'])
def authenticate():
    data=request.get_data(as_text=True)

    try:
        data=json.loads(data)
        if check_creds(data["user"],data["password"]):
            return jsonify({"SSID":assign_ses_ID(add=data["user"])})
        else:
            return jsonify({"Error":"Invalid credentials"}),400

    except:
        return jsonify({"Error":"Invalid json"}),400


@app.route('/logout', methods=['POST'])
def logout():
    data=request.get_data(as_text=1)

    try:
        data=json.loads(data)
        assign_ses_ID(remove=data["SSID"])
        return jsonify({"Status":"Ok"})
    except:
        return jsonify({"Error":"Invalid json"}),400


@app.route('/books',methods=['GET'])
def books():
    try:
        count=request.args.get('count')
        r=get_books(count)

        if r==1:
            return jsonify({"Error":"Count should be a number"})
        else:
            return jsonify({"books":r})

    except:
        return jsonify({"Error":"Invalid json"}),400


@app.route('/book_pdf',methods=['POST'])
def book_pdf():
    data=request.get_data(as_text=1)

    try:
        data=json.loads(data)
        SSID=data['SSID']
        Bid=data['Bid']

        f=get_pdfs(SSID,Bid)
        if f==1:
            return jsonify({"Error":"Bid should be a number"})
        if f==2:
            return jsonify({"Error":"Bid does not exist"})
        if f==3:
            return jsonify({"Error":"Invalid SSID"})
        else:
            return jsonify({"URL":f})

    except KeyError:
        return jsonify({"Error":"SSID or Bid not provided"})
 
    except:
        return jsonify({"Error":"Invalid json"}),400


@app.route('/add', methods=['PUT'])
def add_book():
    data=request.get_data(as_text=1)
    print(data)

    try:
        data=json.loads(data)
        SSID=data['SSID']
        URL=data['URL']
        name=data['name']

        if name=='':
            return jsonify({"Error":"Invalid Book name"})
        if URL=='':
            return jsonify({"Error":"Invalid Book URL"})

        f=add(SSID,URL,name)
        if f==0:
            return jsonify({"Error":"Invalid SSID"})
        else:
            return jsonify({"Bid":f})

    except KeyError:
        return jsonify({"Error":"SSID or URL not provided"})
 
    except:
        return jsonify({"Error":"Invalid json"}),400


@app.route('/remove', methods=['DELETE'])
def remove():
    data=request.get_data(as_text=1)

    try:
        data=json.loads(data)
        SSID=data['SSID']
        Bid=data['Bid']

        f=remove_book(SSID,Bid)
        if f==1:
            return jsonify({"Error":"Bid should be a number"})
        if f==2:
            return jsonify({"Error":"Invalid SSID"})
        else:
            return jsonify({"Status":"Ok"})

    except KeyError:
        return jsonify({"Error":"SSID or Bid not provided"})
 
    except:
        return jsonify({"Error":"Invalid json"}),400
