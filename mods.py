import sqlite3
import secrets


def check_creds(user,passw):
    con=sqlite3.connect('db.db')
    cur=con.cursor()

    query = "SELECT * FROM creds WHERE user=? and passw=?"

    r=cur.execute(query,(user,passw))

    if r.fetchone() !=[]:
        cur.close()
        return 1
    else:
        cur.close()
        return 0


def assign_ses_ID(add=0,remove=0):
    con=sqlite3.connect('db.db')
    cur=con.cursor()

    if add!=0:
        SSID=str(secrets.token_hex(32))
        query='INSERT INTO SSID VALUES(\''+SSID+'\')'
        r=cur.execute(query)
        con.commit()
        cur.close()
        return SSID

    if remove!=0:
        query='DELETE FROM SSID where sid=\''+remove+'\''
        r=cur.execute(query)
        con.commit()
        cur.close()

def get_books(count):
    try:
        count=int(count)
    except:
        return 1

    con=sqlite3.connect('db.db')
    cur=con.cursor()

    r=cur.execute('SELECT Bid,name from books')
    books=r.fetchall()[0:count]
    parsed_json={}

    for _ in books:
        parsed_json[_[0]]=_[1]

    return parsed_json

def get_pdfs(SSID,Bid):
    try:
        Bid=int(Bid)
    except:
        return 1

    con=sqlite3.connect('db.db')
    cur=con.cursor()
    r=cur.execute(f'SELECT sid from SSID WHERE sid=\'{SSID}\'')
    if r.fetchone() != []:
        r=cur.execute(f'SELECT URL from books where Bid={Bid}')
        url=r.fetchone()[0]
        if url=='':
            return 2
        else:
            return url
    else:
        return 3

def remove_book(SSID,Bid):
    try:
        Bid=int(Bid)
    except:
        return 1

    con=sqlite3.connect('db.db')
    cur=con.cursor()
    r=cur.execute(f'SELECT sid from SSID WHERE sid=\'{SSID}\'')
    if r.fetchone() != []:
        r=cur.execute(f'DELETE from books where Bid={Bid}')
        con.commit()
        return 3
    else:
        return 2

def add(SSID,URL,name):
    con=sqlite3.connect('db.db')
    cur=con.cursor()
    r=cur.execute(f'SELECT sid from SSID WHERE sid=\'{SSID}\'')

    if r.fetchone() != []:
        r=cur.execute(f'SELECT MAX(Bid) from books')
        num=int(r.fetchone()[0])+1
        r=cur.execute(f'INSERT into books values ({num},\'{name}\',\'{URL}\')')
        con.commit()
        return num
    else:
        return 0
